$(function(){
    $(".ajaxForm").submit(function(e){
        e.preventDefault();
        var href = $(this).attr("action");
        $.ajax({
            type: "POST",
            dataType: "json",
            url: href,
            data: $(this).serialize(),
            success: function(response){
                if(response.status == "success"){
                    alert("We received your submission, thank you!");
                }
                else{
                    alert("An error occured: " + response.message);
                }
            }
        });
    });
});
$(document).ready(function($) {
	$('.popup-open').click(function() {
		$('.popup-fade').fadeIn();
		return false;
	});	
	
	$('.popup-close').click(function() {
		$(this).parents('.popup-fade').fadeOut();
		return false;
	});		
	$('.popup-fade').click(function(e) {
		if ($(e.target).closest('.popup').length == 0) {
			$(this).fadeOut();					
		}
	});
});
window.addEventListener('DOMContentLoaded', function (event){
    let buttons = $(".btn")
    let flag = document.getElementById('check1')
    if(flag.checked){
        buttons.filter(".disabled").removeClass("disabled").attr('disabled', false)
    }
});
document.getElementById('formButton').addEventListener('click', e => {
    history.pushState({page: 1}, "title 1", "?modal");
});
document.getElementById('closeButton').addEventListener('click', e => {
    history.back();
});
